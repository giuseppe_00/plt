package plt;


public class Traduttore {

	private String frase;
	public static final String NIL="nil";

	public Traduttore(String infrase) {
		// TODO Auto-generated constructor stub
		frase=infrase;
	}

	public String getFrase()
	{
		return frase;
	}

	public String frasePunteggiatura()
	{
		String pun=null;
		String parola=null;
		String fraseP="";
		String car=null;
		
		if(frase.contains(" "))
		{
			car=" ";	
		}
		else
		{
			if(frase.contains("-"))
			{
				car="-";
			}
		}
		
		if((frase.split(" ")).length > 1 || (frase.split("-")).length > 1)
		{
				
			String[] part=frase.split(car);
			
			
			if(checkPunteggiatura(part[0]))
			{
				pun=part[0].substring(part[0].length());
				parola=part[0].substring(0,part[0].length());
				Traduttore Tradu=new Traduttore(parola);
				fraseP+=Tradu.traduci()+pun;
			}
			else
			{
				Traduttore Tradu=new Traduttore(part[0]);
				fraseP+=Tradu.traduci();
			}
			
			for(int i=1;i<part.length;i++)
			{
				if(checkPunteggiatura(part[i]))
				{
					pun=part[i].substring(part[0].length());
					parola=part[i].substring(0,part[0].length());
					Traduttore Tradu=new Traduttore(parola);
					fraseP+=car+Tradu.traduci()+pun;
				}
				else
				{
					Traduttore Tradu=new Traduttore(part[i]);
					fraseP+=car+Tradu.traduci();
				}
			}
			
			return fraseP;
		}
		else
		{
			pun=frase.substring(frase.length());
			parola=frase.substring(0,frase.length());
			Traduttore traduttore=new Traduttore(parola);
			fraseP=traduttore.traduci() + pun;
			return fraseP;
		}
		
	}
	public String fraseSplit()
	{
		String car=null;
		String nFrase=null;
		if(frase.contains(" "))
		{
			car=" ";	
		}
		else
		{
			if(frase.contains("-"))
			{
				car="-";
			}
		}
		String[] parole=frase.split(car);
		Traduttore tradu = new Traduttore(parole[0]);
		nFrase=tradu.traduci();
		

		for(int i=1;i<parole.length;i++)
		{
			Traduttore trad1=new Traduttore(parole[i]);
			nFrase+=car +trad1.traduci();
		}

		return nFrase;

	}

	public String traduci()
	{
		if(frase.length()>0)
		{  
			if(checkPunteggiatura(frase))
			{
				return frasePunteggiatura();
			}
			else
			{
				if((frase.split(" ")).length > 1 || (frase.split("-")).length > 1)
				{
					return fraseSplit();
				}
				else
				{
					if(inizioVocale())
					{
						if(frase.endsWith("y"))
						{
							return frase + "nay";
						}
						else {
							if(fineVocale())
							{
								return frase + "yay";
							}
							else {
								if(fineConsonante())
								{
									return frase + "ay";
								}  
							}
						}
					}
					else
					{
						if(!inizioVocale())
						{
							String app = frase.substring(1, frase.length()) + frase.charAt(0);

							do {

								if(checkConsonante(app))
								{
									app = app.substring(1, app.length()) + app.charAt(0);
								}

							}while(checkConsonante(app));

							return app + "ay";
						}
					}
				}
			}
			
		}
		return NIL;

	}


	public boolean inizioVocale()
	{
		return frase.startsWith("a") || frase.startsWith("e") || frase.startsWith("i") || frase.startsWith("o") || frase.startsWith("u");

	}

	public boolean fineVocale()
	{
		return  frase.endsWith("a") || frase.endsWith("e") || frase.endsWith("i") || frase.endsWith("o") || frase.endsWith("u");

	}

	public boolean fineConsonante()
	{
		return !frase.endsWith("a") && !frase.endsWith("e") && !frase.endsWith("i") && !frase.endsWith("o") && !frase.endsWith("u");

	}
	public boolean checkConsonante(String inFrase)
	{
		return !inFrase.startsWith("a") && !inFrase.startsWith("e") && !inFrase.startsWith("i") && !inFrase.startsWith("o") && !inFrase.startsWith("u");
	}
    public boolean checkPunteggiatura(String inParola)
    {
    	if(inParola.endsWith(".") || inParola.endsWith(",") || inParola.endsWith(";") || inParola.endsWith(":") || inParola.endsWith("?") || inParola.endsWith("!") || inParola.endsWith("(") || inParola.endsWith(")") || inParola.endsWith("[") || inParola.endsWith("]") || inParola.endsWith("{") || inParola.endsWith("}"))
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
}

