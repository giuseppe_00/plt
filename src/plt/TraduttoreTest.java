package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TraduttoreTest {

	@Test
	public void testInputPhrase() {
		String input="Hello World";
		Traduttore trad=new Traduttore(input);
		assertEquals("Hello World",trad.getFrase());
	}

	@Test
	public void testInputEmpty() {
		String input="";
		Traduttore trad=new Traduttore(input);
		assertEquals("nil",trad.traduci());
	}
	
	@Test
	public void testInizioVocali() {
		String input="any";
		Traduttore trad=new Traduttore(input);
		assertEquals("anynay",trad.traduci());
	}
	@Test
	public void testFinaleY() {
		String input="any";
		Traduttore trad=new Traduttore(input);
		assertEquals("anynay",trad.traduci());
	}
	@Test
	public void testFinaleVocale() {
		String input="apple";
		Traduttore trad=new Traduttore(input);
		assertEquals("appleyay",trad.traduci());
	}
	@Test
	public void testFinaleConsonante() {
		String input="ask";
		Traduttore trad=new Traduttore(input);
		assertEquals("askay",trad.traduci());
	}
	
	@Test
	public void testInizioConsonante() {
		String input="hello";
		Traduttore trad=new Traduttore(input);
		assertEquals("ellohay",trad.traduci());
	}
	@Test
	public void testInizioConsonanti() {
		String input="known";
		Traduttore trad=new Traduttore(input);
		assertEquals("ownknay",trad.traduci());
	}
	@Test
	public void testFraseSpazio() {
		String input="hello world";
		Traduttore trad=new Traduttore(input);
		assertEquals("ellohay orldway",trad.traduci());
	}
	@Test
	public void testFraseTrattino() {
		String input="well-being";
		Traduttore trad=new Traduttore(input);
		assertEquals("ellway-eingbay",trad.traduci());
	}
	@Test
	public void testFrasePunteggiatura() {
		String input="hello world!";
		Traduttore trad=new Traduttore(input);
		assertEquals("ellohay orldway!",trad.traduci());
	}
	@Test
	public void testFrasePunteggiaturaTrattino() {
		String input="hello-world!";
		Traduttore trad=new Traduttore(input);
		assertEquals("ellohay-orldway!",trad.traduci());
	}
}
